let $ = jQuery;

import characters from "../ressources/characters.json" assert { type: "json" };

(function () {
  $(".contact").hide();
  let selectedCharacter = "";

  const displaySearchResultsNumber = (arr) => {
    if ($("#search-character").val().length > 3) {
      arr.length === 0
        ? $("#search-results-number")
            .text("Aucun résultat pour votre recherche")
            .show()
        : $("#search-results-number")
            .text(
              `${arr.length} personnage${arr.length > 1 ? "s" : ""} sur ${
                characters.length
              }`
            )
            .show();
    } else {
      $("#search-results-number").text("").hide();
    }
  };

  const generateCharacters = () => {
    let searchVal = $("#search-character").val();
    let sortBy = $("#sort-by").val();
    let charactersToDisplay = characters;

    $(".characters").empty();

    if ($("#search-character").val().length > 3) {
      charactersToDisplay = characters.filter(
        (character) =>
          character.name.toLowerCase().includes(searchVal.toLowerCase()) ||
          character.surname.toLowerCase().includes(searchVal.toLowerCase())
      );

      if (
        charactersToDisplay.length === 0 ||
        !charactersToDisplay.some(
          (character) =>
            `${character.surname.toLowerCase()}-${character.name.toLowerCase()}` ===
            selectedCharacter
        )
      ) {
        $(".contact").hide();
        $("#selected-character").val("");
        selectedCharacter = "";
      }
    }

    if (sortBy === "asc") {
      charactersToDisplay.sort((a, b) => {
        let nameA = a.surname.toLowerCase() + a.name.toLowerCase();
        let nameB = b.surname.toLowerCase() + b.name.toLowerCase();

        return nameA < nameB ? -1 : nameA > nameB ? 1 : 0;
      });
    } else if (sortBy === "desc") {
      charactersToDisplay.sort((a, b) => {
        let nameA = a.surname.toLowerCase() + a.name.toLowerCase();
        let nameB = b.surname.toLowerCase() + b.name.toLowerCase();

        return nameA > nameB ? -1 : nameA < nameB ? 1 : 0;
      });
    }

    charactersToDisplay.forEach((element) => {
      generateCharacter(element);
    });

    displaySearchResultsNumber(charactersToDisplay);
  };

  const generateCharacter = (character) => {
    let isSelected =
      selectedCharacter ===
      `${character.surname.toLowerCase()}-${character.name.toLowerCase()}`;

    let templateCharacter = `<div class="character${
      isSelected ? " selected" : ""
    }" data-name="${character.name}" data-surname="${character.surname}">
            <img src="./assets/images/${character.image}" height="400" />
            <p>${character.surname} ${character.name}</p>
        </div>`;

    $(".characters").append(templateCharacter);
  };

  generateCharacters(characters);

  $("#search-character").on("keyup search", generateCharacters);

  $("#sort-by").on("change", generateCharacters);

  $(document).on("click", ".character", function () {
    if ($(this).hasClass("selected")) {
      $(this).removeClass("selected");
      $(".contact").hide();
      $("#selected-character").val("");
    } else {
      selectedCharacter = `${$(this).data("surname").toLowerCase()}-${$(this)
        .data("name")
        .toLowerCase()}`;
      $(".character").removeClass("selected");
      $(this).addClass("selected");
      $(".contact").show(500);
      $("#selected-character").val(
        $(this).data("surname") + $(this).data("name")
      );
    }
  });
})($);
